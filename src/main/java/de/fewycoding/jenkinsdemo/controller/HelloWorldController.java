package de.fewycoding.jenkinsdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloWorldController {

    @GetMapping("/hello")
    public String helloWorld(){
        System.out.println("Hallo welt");
        return "Hallo Mein Freund du hast die Jenkinsdemo App erfolgreich gestartet";
    }
}
