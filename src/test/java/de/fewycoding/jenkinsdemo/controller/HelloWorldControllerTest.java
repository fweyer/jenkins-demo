package de.fewycoding.jenkinsdemo.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloWorldControllerTest {

    @Test
    public void testhello(){
        HelloWorldController helloWorldController = new HelloWorldController();
        String result = helloWorldController.helloWorld();
        assertEquals(result,"Hallo Mein Freund du hast die Jenkinsdemo App erfolgreich gestartet");
    }

}